<!--
    Alumna: García Rangel Isis Violeta
    Curso: Lenguaje de programación PHP
    Ejercicio: Expresiones Regulares con PHP
-->
<html>
    <head>
        <title>Expresiones Regulares</title>
    </head>

    <style>
    .centro{
    position: absolute;
    content: "";
    left: 650px;
    right: 650px;
    top: 0px;
    }

    .text-center{
    text-align: center !important;
    }

    .normaltext{
    font-family: Arial, Helvetica, sans-serif;
    color: #000000;
    font-size: 14px;
    }

    #rectan {
    width: 650px; 
    height: 850px; 
    border: 1px solid #000000;
    background: #0DACF1;
    }

    </style>

    <body>
        <div class = 'container centro text-center normaltext'>
        <div id="rectan">
        <br></br>

        <!-- Expresión regular que detecta el correo electrónico -->

        <?php
            echo ("<h4>Esta expresión regular devuelve '1' si el email esta en el formato correcto:</h4>");
            echo "Prueba 1: VioletVitelli@gmail.com";
            echo("</br>"); 
            $email = 'VioletVitelli@gmail.com';
            echo preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})$/",$email );
            echo("</br>"); 
            echo "Prueba 2: gatito2@@gmail.com";
            echo("</br>"); 
            $email1 = 'gatito2@@gmail.com';
            echo preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})$/",$email1 );
            echo("</br>"); 
            echo "El email no se encuentra dentro del formato, por eso devuelve '0'.";
        ?>

        <br></br> 
  
        <!-- Expresión regular que detecta el CURP -->

        <?php
            echo ("<h4>Esta expresión regular devuelve '1' si el CURP esta en el formato correcto:</h4>");
            echo "Prueba 1: ABCD123456EFGHIJ78";
            echo("</br>");
            $curp = 'ABCD123456EFGHIJ78';
            echo preg_match("/^([A-Z{4}])+([0-9]{6})+([A-Z{4}])+([0-9]{2})$/", $curp );
            echo("</br>"); 
            echo "Prueba 2: GARI990911MDFRNS07";
            echo("</br>");
            $curp1 = 'GARI990911MDFRNS07';
            echo preg_match("/^([A-Z{4}])+([0-9]{6})+([A-Z{4}])+([0-9]{2})$/", $curp1 );
            $disqueCurp = 'GARI99091100000030';
            echo("</br>"); 
            echo "Prueba 3: GARI99091100000030";
            echo("</br>");
            echo preg_match("/^([A-Z{4}])+([0-9]{6})+([A-Z{4}])+([0-9]{2})$/", $disqueCurp );
            echo("</br>");
            echo "El CURP no se encuentra dentro del formato, por eso devuelve '0'.";

        ?>
        <br></br>

        <!-- Expresión regular que detecta palabras de longitud mayor a 50, formadas solo por letras -->

        <?php
            echo ("<h4>Esta expresión regular devuelve '1' si se detecta palabras de longitud mayor a 50, formadas solo por letras:</h4>");
            echo "Prueba 1:  ContrarrevolucionariamenteHiperesternocleidomastoideitis";
            echo("</br>");
            $enunciado = 'ContrarrevolucionariamenteHiperesternocleidomastoideitis'; //junte dos palabras pa alcanzar la mayor longitud posible
            echo preg_match("/^([a-zA-ZñÑáéíóúÁÉÍÓÚ]{50,100})$/", $enunciado );
            echo("</br>"); 
            echo "Prueba 2:  ContrarrevolucionariamenteHiperesternocleidomastoideitis10203046//***";
            echo("</br>");
            $enunciado1 = 'ContrarrevolucionariamenteHiperesternocleidomastoideitis10203046//***';
            echo preg_match("/^([*a-zA-ZñÑáéíóúÁÉÍÓÚ]{50,100})$/", $enunciado1 );
            echo("</br>");
            echo "La palabra no se encuentra dentro del formato, por eso devuelve '0'.";
        ?>

        <br></br>

        <!-- Expresión regular que escapa de los símbolos especiales -->

        <?php
            echo ("<h4>Esta expresión regular devuelve '0' si se escapa de los símbolos especiales:</h4>");
            echo "Prueba 1:  hola/holu/holi/holo";
            echo("</br>");
            $caracteres = 'hola/holu/holi/holo';
            echo preg_match("/^([\[\]\(\)\{\}\.\*\+\?\^\$\\\|])$/", $caracteres  );
            echo("</br>"); 
            echo "Prueba 2:  *******[](){}.+?^$\|";
            echo("</br>"); 
            $carac1 = '*******[](){}.+?^$\|';
            echo preg_match("/^([\[\]\(\)\{\}\.\*\+\?\^\$\\\|])$/", $carac1  );
            echo("</br>");
            echo "Ambos devuelven '0' porque estan haciendo uso de símbolos especiales.";
        ?>
        <br></br>


        <!-- Expresión regular que detecta números decimales -->

        <?php
            echo ("<h4>Esta expresión regular devuelve '1' si detecta números decimales:</h4>");
            echo "Prueba 1:  10.3";
            echo("</br>");
            $decimales = '10.3';
            echo preg_match("/^([0-9]+([.][0-9]))$/", $decimales  );
            echo("</br>");
            echo "Prueba 2:  1";
            echo("</br>");
            $decimales = '1';
            echo preg_match("/^([0-9]+([.][0-9]))$/", $decimales  );
            echo("</br>");
            echo "Devuelve '0' porque no cumple el formato.";

        ?>
        </div>
        </div>
    </body>
</html>