<!--
    Alumna: García Rangel Isis Violeta
    Curso: Lenguaje de programación PHP
    Ejercicio: Pirámide y rombo
-->
<html>
    <head>
        <title>Pirámide y rombo</title>
    </head>

    <body>
        <div align = 'center'>
        <?php
            $h = 30; //máximo de filas que queremos
            $f = 1;
            $i;

            /*Dibujo del triángulo*/

            while($f <= $h){
                for($i = 1; $i <= $f; $i++){
                
                    echo "*";

                }
                echo("<br/>");
                $f++;
            }

            echo("<br/>");
            echo("<br/>");

            /*Dibujo del rombo*/

            $f = 1;
            $i;

            while($f <= $h){
                for($i = 1; $i <= $f; $i++){
                
                    echo "*";

                }
                echo("<br/>");
                $f++;
            }

            $f = 1;
            $i;
            while($f < $h){
                for($i = 29; $i >= $f; $i--){ //cambia el ciclo for para invertir el triángulo
                
                    echo "*";

                }
                echo("<br/>");
                $f++;
            }

        ?>
        </div>
    </body>


</html>